# EgyRadio

## Description

App helps you to listen to all radio stations from Egypt ( from online streams ) and can choose favorite radio channel by storing  it in a database and you have speed list to make changing radio station more easer

## Screen Shot

![](ScreenShots/imgonline-com-ua-twotoone-6HgmtsfP0Zy.jpg)

## Third Party Libraries

* [Picasso](http://square.github.io/picasso/)
* [ExoPlayer](https://github.com/google/ExoPlayer)
* [Parceler](https://github.com/johncarl81/parceler)
* [MovingImageView](https://github.com/AlbertGrobas/MovingImageView)
* [SmartTabLayout](https://github.com/ogaclejapan/SmartTabLayout)

