package com.oze.emad.egyradio.Model.Local;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by 3mad on 25-Aug-17.
 */

public class DbContract {

    public static final String AUTHORITY = "com.oze.emad.egyradio";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);
    public static final String PATH_FAVORITE_RADIO_STATIONS = "FavouriteRadioStations";
    public static final String PATH_SPEED_LIST_RADIO_STATIONS = "SpeedListRadioStations";

    public static class RadioStation implements BaseColumns {

        public static final Uri CONTENT_FAVORITE_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_FAVORITE_RADIO_STATIONS).build();
        public static final Uri CONTENT_SPEED_LIST_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_SPEED_LIST_RADIO_STATIONS).build();

        public static final String[] ALL_COLUMNS = {
                RadioStation.COLUMN_NAME,
                RadioStation.COLUMN_STREAM_URL,
                RadioStation.COLUMN_WEB_SITE_URL,
                RadioStation.COLUMN_LOGO_URL,
                RadioStation.COLUMN_LOGO_BACKGROUND_COLOR,
                RadioStation.COLUMN_BACKDROP_URL,
                RadioStation.COLUMN_BACKDROP_BACKGROUND_COLOR,
                RadioStation.COLUMN_DESCRIPTION
        };

        public static final String FAVOURITE_TABLE_NAME = "FavouriteRadioStations";
        public static final String SPEED_LIST_TABLE_NAME = "SpeedListRadioStations";
        public static final String COLUMN_NAME = "Name";
        public static final String COLUMN_STREAM_URL = "StreamUrl";
        public static final String COLUMN_WEB_SITE_URL = "WebSiteUrl";
        public static final String COLUMN_BACKDROP_URL = "BackgroundUrl";
        public static final String COLUMN_LOGO_URL = "ImageUrl";
        public static final String COLUMN_DESCRIPTION = "Description";
        public static final String COLUMN_BACKDROP_BACKGROUND_COLOR = "BackdropBackgroundColor";
        public static final String COLUMN_LOGO_BACKGROUND_COLOR = "LogoBackgroundColor";


    }
}
