package com.oze.emad.egyradio.RadioActivity.SpeedList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oze.emad.egyradio.R;
import com.oze.emad.egyradio.Utilies.RadioStation;

import java.util.ArrayList;

/**
 * Created by 3mad on 23-Aug-17.
 */

public class SpeedListRecyclerViewAdepter extends RecyclerView.Adapter<SpeedListRecyclerViewAdepter.ViewHolder> {


    private SpeedListRecyclerViewAdepter.ItemListener mItemListener;
    private ArrayList<RadioStation> mRadioStations;
    private Context mContext;

    public SpeedListRecyclerViewAdepter(SpeedListRecyclerViewAdepter.ItemListener itemListener, ArrayList<RadioStation> radioStations, Context context) {
        this.mItemListener = itemListener;
        mRadioStations = radioStations;
        mContext = context;
    }

    interface ItemListener {
        void OnClickListener(int position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.speed_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.stationName.setText(mRadioStations.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return mRadioStations.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView stationName;

        public ViewHolder(View itemView) {
            super(itemView);
            stationName = (TextView) itemView.findViewById(R.id.speed_list_station_name);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mItemListener.OnClickListener(getLayoutPosition());
                }
            });
        }
    }
}
