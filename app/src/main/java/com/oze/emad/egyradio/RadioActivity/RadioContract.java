package com.oze.emad.egyradio.RadioActivity;

/**
 * Created by 3mad on 23-Aug-17.
 */

public interface RadioContract {
    interface View{
        void showRadioBackground(String backgroundUrl);
        void showRadioCover(String coverUrl);
        void initializeRadioStationWebSiteButton(String webSiteUrl);
        void initializeFavoriteButton();
        void showFavoriteIcon();
        void showUnFavoriteIcon();
        void initializePlayPauseButton();
        void showPlayIcon();
        void showPauseIcon();
        void initializeVolumeBar();
        void updateVolumeBar(int v);
        void updateVolumeIcon(int v,int max);
        void initializeVolumeIcon();
        void showVolumeMuteIcon();
        void showSnakeBar(String massage);
    }
    interface Presenter{
        void start();
        void addToFavorite();
        boolean isFavorite();
        void favoriteButtonAction();
        void removeFromFavorite();
        void buttonPlayPauseAction();
        void volumeChanged();
        void changeVolume(int v);
        void prepareRadioStream();
        void stop();
    }
}
