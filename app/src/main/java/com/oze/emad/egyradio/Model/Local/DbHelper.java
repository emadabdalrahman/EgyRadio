package com.oze.emad.egyradio.Model.Local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by 3mad on 25-Aug-17.
 */

public class DbHelper extends SQLiteOpenHelper {


    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "RadioStations.db";

    public static final String SQL_CREATE_FAVORITE_RADIO_STATIONS_TABLE =
            "CREATE TABLE " + DbContract.RadioStation.FAVOURITE_TABLE_NAME + " (" +
                    DbContract.RadioStation.COLUMN_NAME + " TEXT," +
                    DbContract.RadioStation.COLUMN_STREAM_URL + " TEXT," +
                    DbContract.RadioStation.COLUMN_WEB_SITE_URL + " TEXT," +
                    DbContract.RadioStation.COLUMN_LOGO_URL + " TEXT," +
                    DbContract.RadioStation.COLUMN_LOGO_BACKGROUND_COLOR + " TEXT," +
                    DbContract.RadioStation.COLUMN_BACKDROP_URL + " TEXT," +
                    DbContract.RadioStation.COLUMN_BACKDROP_BACKGROUND_COLOR + " TEXT," +
                    DbContract.RadioStation.COLUMN_DESCRIPTION + " TEXT" + ")";

    public static final String SQL_CREATE_SPEED_LIST_RADIO_STATIONS_TABLE =
            "CREATE TABLE " + DbContract.RadioStation.SPEED_LIST_TABLE_NAME + " (" +
                    DbContract.RadioStation.COLUMN_NAME + " TEXT," +
                    DbContract.RadioStation.COLUMN_STREAM_URL + " TEXT," +
                    DbContract.RadioStation.COLUMN_WEB_SITE_URL + " TEXT," +
                    DbContract.RadioStation.COLUMN_LOGO_URL + " TEXT," +
                    DbContract.RadioStation.COLUMN_LOGO_BACKGROUND_COLOR + " TEXT," +
                    DbContract.RadioStation.COLUMN_BACKDROP_URL + " TEXT," +
                    DbContract.RadioStation.COLUMN_BACKDROP_BACKGROUND_COLOR + " TEXT," +
                    DbContract.RadioStation.COLUMN_DESCRIPTION + " TEXT" + ")";

    public static final String SQL_DELETE_FAVORITE_RADIO_STATIONS_TABLE = "DROP TABLE IF EXISTS" + DbContract.RadioStation.FAVOURITE_TABLE_NAME;
    public static final String SQL_DELETE_SPEED_LIST_RADIO_STATIONS_TABLE = "DROP TABLE IF EXISTS" + DbContract.RadioStation.SPEED_LIST_TABLE_NAME;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_FAVORITE_RADIO_STATIONS_TABLE);
        db.execSQL(SQL_CREATE_SPEED_LIST_RADIO_STATIONS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(SQL_DELETE_FAVORITE_RADIO_STATIONS_TABLE);
        db.execSQL(SQL_DELETE_SPEED_LIST_RADIO_STATIONS_TABLE);

        onCreate(db);
    }
}
