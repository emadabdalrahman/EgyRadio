package com.oze.emad.egyradio.MainAcivity;

/**
 * Created by 3mad on 22-Sep-17.
 */

public interface BottomRadioPlayerListener {
    int getBottomRadioPlayerHeight();

    void setBottomRadioPlayerTranslationY(float translationY);

    float getBottomRadioPlayerTranslationY();
}
