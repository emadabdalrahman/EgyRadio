package com.oze.emad.egyradio.RadioActivity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.bumptech.glide.Glide;
import com.github.florent37.glidepalette.BitmapPalette;
import com.github.florent37.glidepalette.GlidePalette;
import com.oze.emad.egyradio.MainAcivity.MainActivity;
import com.oze.emad.egyradio.R;
import com.oze.emad.egyradio.Utilies.RadioStation;
import com.squareup.picasso.Picasso;

import net.grobas.view.MovingImageView;

import org.parceler.Parcels;

import static com.oze.emad.egyradio.MainAcivity.MainActivity.sRadioStreamController;
import static com.oze.emad.egyradio.MainAcivity.MainActivity.sSharedPreferences;


/**
 * Created by 3mad on 23-Aug-17.
 */

public class RadioFragment extends Fragment implements RadioContract.View {

    private FragmentActivity mContext;
    private View mRootView;
    private RadioPresenter mRadioPresenter;
    private RadioStation mRadioStation;
    private ImageButton mFavoriteButton;
    private BottomSheetBehavior mBottomSheetBehavior;
    private ImageButton mPlayPauseButton;
    private SeekBar mVolumeBar;
    private AudioManager mAudioManager;
    private ImageButton mVolumeIcon;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = (FragmentActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.fragment_radio, container, false);

        mRadioStation = Parcels.unwrap(getArguments().getParcelable("RadioStation"));

        mRadioPresenter = new RadioPresenter(this, mRadioStation, mContext);
        mRadioPresenter.start();

        return mRootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mRadioPresenter.stop();
    }

    @Override
    public void onResume() {
        super.onResume();
        mRadioPresenter.updatePlayPauseIcon();
    }

    @Override
    public void showRadioBackground(String backgroundUrl) {

        MovingImageView image = (MovingImageView) mRootView.findViewById(R.id.radio_background);
        image.getMovingAnimator().setInterpolator(new DecelerateInterpolator());
        image.getMovingAnimator().setSpeed(10);
        image.setMaxRelativeSize(50);
        image.getMovingAnimator().addCustomMovement().
                addHorizontalMoveToLeft().
                addHorizontalMoveToRight().
                start();

        image.setBackgroundColor(Color.parseColor(mRadioStation.getBackdropBackgroundColor()));

        Picasso.with(mContext)
                .load(backgroundUrl)
                .placeholder(R.drawable.logo1)
                .into(image);

//
//        Glide.with(this).load(backgroundUrl)
//                .listener(GlidePalette.with(backgroundUrl)
//                        .use(GlidePalette.Profile.VIBRANT)
//                        .intoBackground(image, BitmapPalette.Swatch.RGB)
//                )
//                .into(image);
        //GlidePalette<Drawable> palette = GlidePalette.with(backgroundUrl).use(GlidePalette.Profile.VIBRANT);


    }

    @Override
    public void showRadioCover(String coverUrl) {
        ImageView radioCover = (ImageView) mRootView.findViewById(R.id.radio_fragment_radio_cover);
        Glide.with(mContext).load(coverUrl).into(radioCover);
    }

    @Override
    public void initializeRadioStationWebSiteButton(final String webSiteUrl) {
        ImageButton webSiteButton = (ImageButton) mRootView.findViewById(R.id.fragment_radio_web_site);
        webSiteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(webSiteUrl));
                startActivity(intent);
            }
        });
    }

    @Override
    public void initializeFavoriteButton() {
        mFavoriteButton = (ImageButton) mRootView.findViewById(R.id.fragment_radio_favorite);

        mFavoriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRadioPresenter.favoriteButtonAction();
            }
        });
    }

    @Override
    public void showFavoriteIcon() {
        mFavoriteButton.setImageResource(R.drawable.ic_favorite_red_48dp);
    }

    @Override
    public void showUnFavoriteIcon() {
        mFavoriteButton.setImageResource(R.drawable.ic_favorite_border_white_48dp);
    }

    @Override
    public void initializePlayPauseButton() {
        mPlayPauseButton = (ImageButton) mRootView.findViewById(R.id.fragment_radio_play_pauseButton);
        mPlayPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRadioPresenter.buttonPlayPauseAction();
            }
        });
    }

    @Override
    public void showPlayIcon() {
        mPlayPauseButton.setImageResource(R.drawable.ic_play_arrow_white_48dp);
    }

    @Override
    public void showPauseIcon() {
        mPlayPauseButton.setImageResource(R.drawable.ic_pause_white_48dp);
    }

    @Override
    public void initializeVolumeBar() {
        mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        mVolumeBar = (SeekBar) mRootView.findViewById(R.id.radio_fragment_seekBar);
        mVolumeBar.setMax(mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));

        if (sRadioStreamController.isVolumeMute()) {
            mVolumeBar.setProgress(sRadioStreamController.getLastVolumeLevel());
            // mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,mRadioPresenter.getLastVolumeLevel(),0);
        } else {
            mVolumeBar.setProgress(mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
        }

        mVolumeBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                sRadioStreamController.setVolumeMute(false);
                mRadioPresenter.changeVolume(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    public void updateVolumeBar(int v) {
        mVolumeBar.setProgress(v);
    }

    @Override
    public void updateVolumeIcon(int v, int max) {
        if (v >= max / 2) {
            mVolumeIcon.setImageResource(R.drawable.ic_volume_up_white_36dp);
        } else if (v < max / 2 && v != 0) {
            mVolumeIcon.setImageResource(R.drawable.ic_volume_down_white_36dp);
        } else {
            mVolumeIcon.setImageResource(R.drawable.ic_volume_off_white_36dp);
        }

    }

    public void showVolumeMuteIcon() {
        mVolumeIcon.setImageResource(R.drawable.ic_volume_off_white_36dp);
    }

    @Override
    public void initializeVolumeIcon() {
        mVolumeIcon = (ImageButton) mRootView.findViewById(R.id.fragment_radio_sound_level_image);
        int v = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        int max = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        updateVolumeIcon(v, max);

        mVolumeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sRadioStreamController.isVolumeMute()) {
                    mRadioPresenter.unMuteVolume();
                } else {
                    mRadioPresenter.muteVolume();
                }
            }
        });
    }

    @Override
    public void showSnakeBar(String massage){
        Snackbar.make(mRootView,massage,Snackbar.LENGTH_LONG).show();
    }
}