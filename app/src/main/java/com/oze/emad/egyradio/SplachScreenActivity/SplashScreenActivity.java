package com.oze.emad.egyradio.SplachScreenActivity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.crashlytics.android.Crashlytics;
import com.oze.emad.egyradio.MainAcivity.MainActivity;
import com.oze.emad.egyradio.R;

import io.fabric.sdk.android.Fabric;

public class SplashScreenActivity extends AppCompatActivity {
    Intent i;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        Fabric.with(this, new Crashlytics());

        i = new Intent(this, MainActivity.class);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

                startActivity(i);

                // close this activity
                finish();
            }
        }, 5000);
    }
}
