package com.oze.emad.egyradio.Utilies;

import org.parceler.Parcel;

/**
 * Created by 3mad on 22-Aug-17.
 */
@Parcel
public class RadioStation {
    private String Name;
    private String Description;
    private String WebSiteUrl;
    private String StreamUrl;
    private String LogoUrl;
    private String BackdropURL;
    private String BackdropBackgroundColor;

    public String getBackdropBackgroundColor() {
        return BackdropBackgroundColor;
    }

    public void setBackdropBackgroundColor(String backdropBackgroundColor) {
        BackdropBackgroundColor = backdropBackgroundColor;
    }

    public String getLogoBackgroundColor() {
        return LogoBackgroundColor;
    }

    public void setLogoBackgroundColor(String logoBackgroundColor) {
        LogoBackgroundColor = logoBackgroundColor;
    }

    private String LogoBackgroundColor;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getWebSiteUrl() {
        return WebSiteUrl;
    }

    public void setWebSiteUrl(String webSiteUrl) {
        WebSiteUrl = webSiteUrl;
    }

    public String getStreamUrl() {
        return StreamUrl;
    }

    public void setStreamUrl(String streamUrl) {
        StreamUrl = streamUrl;
    }

    public String getLogoUrl() {
        return LogoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        LogoUrl = logoUrl;
    }

    public String getBackdropURL() {
        return BackdropURL;
    }

    public void setBackdropURL(String backdropURL) {
        BackdropURL = backdropURL;
    }
}
