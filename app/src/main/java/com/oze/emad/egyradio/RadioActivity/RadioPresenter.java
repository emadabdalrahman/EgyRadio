package com.oze.emad.egyradio.RadioActivity;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.database.Cursor;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;

import com.oze.emad.egyradio.Model.Local.DbContract;
import com.oze.emad.egyradio.Utilies.Network;
import com.oze.emad.egyradio.Utilies.RadioStation;


import static com.oze.emad.egyradio.MainAcivity.FavouriteFragment.FavouriteFragment.sNotifyDbChanged;
import static com.oze.emad.egyradio.MainAcivity.MainActivity.sRadioStreamController;
import static com.oze.emad.egyradio.MainAcivity.MainActivity.sSharedPreferences;

/**
 * Created by 3mad on 23-Aug-17.
 */

public class RadioPresenter implements RadioContract.Presenter {

    private RadioContract.View mView;
    private RadioStation mRadioStation;
    private Context mContext;
    private SharedPreferences.Editor mEditor;
    private AudioManager mAudioManager;
    private SettingsContentObserver mSettingsContentObserver;
    private NetworkStateChanged mNetworkStateChanged;
    public boolean mStartConnected;

    public RadioPresenter(RadioContract.View view, RadioStation radioStation, Context context) {
        mContext = context;
        mView = view;
        mRadioStation = radioStation;
    }

    @Override
    public void start() {

        if (!Network.isConnected(mContext)) {
            mView.showSnakeBar("No Internet Connection");
            mStartConnected = false;
        }else {
            prepareRadioStream();
            mStartConnected = true;
        }

        mView.showRadioBackground(mRadioStation.getBackdropURL());
        mView.showRadioCover(mRadioStation.getLogoUrl());
        mView.initializeRadioStationWebSiteButton(mRadioStation.getWebSiteUrl());
        mView.initializeFavoriteButton();
        mView.initializePlayPauseButton();
        mView.initializeVolumeBar();
        mView.initializeVolumeIcon();

        if (isFavorite()) {
            mView.showFavoriteIcon();
        }

        if (sRadioStreamController.isPlayed() && sRadioStreamController.getPlayStreamUrl().equals(mRadioStation.getStreamUrl())) {
            mView.showPauseIcon();
        }

        mEditor = sSharedPreferences.edit();
        mSettingsContentObserver = new SettingsContentObserver(new Handler());
        mNetworkStateChanged = new NetworkStateChanged();
        mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);

        registerContentObserver();
        registerBroadcastReceiver();

    }

    @Override
    public void addToFavorite() {
        ContentValues values = new ContentValues();

        values.put(DbContract.RadioStation.COLUMN_NAME, mRadioStation.getName());
        values.put(DbContract.RadioStation.COLUMN_STREAM_URL, mRadioStation.getStreamUrl());
        values.put(DbContract.RadioStation.COLUMN_WEB_SITE_URL, mRadioStation.getWebSiteUrl());
        values.put(DbContract.RadioStation.COLUMN_LOGO_URL, mRadioStation.getLogoUrl());
        values.put(DbContract.RadioStation.COLUMN_BACKDROP_URL, mRadioStation.getBackdropURL());
        values.put(DbContract.RadioStation.COLUMN_DESCRIPTION, mRadioStation.getDescription());
        values.put(DbContract.RadioStation.COLUMN_BACKDROP_BACKGROUND_COLOR, mRadioStation.getBackdropBackgroundColor());
        values.put(DbContract.RadioStation.COLUMN_LOGO_BACKGROUND_COLOR, mRadioStation.getLogoBackgroundColor());

        mContext.getContentResolver().insert(DbContract.RadioStation.CONTENT_FAVORITE_URI, values);

    }

    @Override
    public boolean isFavorite() {
        Cursor cursor = mContext.getContentResolver().query(DbContract.RadioStation.CONTENT_FAVORITE_URI, null, null, null, null);
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                if (cursor.getString(cursor.getColumnIndex(DbContract.RadioStation.COLUMN_NAME)).equals(mRadioStation.getName())) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void favoriteButtonAction() {
        if (!isFavorite()) {
            addToFavorite();
            mView.showFavoriteIcon();
        } else {
            removeFromFavorite();
            mView.showUnFavoriteIcon();
        }

        if (!sNotifyDbChanged) {
            sNotifyDbChanged = true;
        }
    }

    @Override
    public void removeFromFavorite() {
        Uri uri = DbContract.RadioStation.CONTENT_FAVORITE_URI.buildUpon().appendPath(mRadioStation.getName()).build();
        mContext.getContentResolver().delete(uri, null, null);
    }

    @Override
    public void prepareRadioStream() {

        if (sRadioStreamController.getPlayStreamUrl() == null) {
            sRadioStreamController.prepareMainExoPlayer(mRadioStation.getStreamUrl());
        }

        if (!sRadioStreamController.getPlayStreamUrl().equals(mRadioStation.getStreamUrl())) {
            sRadioStreamController.prepareSideExoPlayer(mRadioStation.getStreamUrl());
        }

    }

    @Override
    public void stop() {
        sRadioStreamController.stopPrepareSideExoPlayer();
        unregisterContentObserver();
        unregisterBroadcastReceiver();
    }

    public void saveAsLastPlayed() {
        mEditor.putString("LastRadioName", mRadioStation.getName());
        mEditor.putString("LastRadioLogoUrl", mRadioStation.getLogoUrl());
        mEditor.putString("LastRadioBackdropURL", mRadioStation.getBackdropURL());
        mEditor.putString("LastRadioStreamUrl", mRadioStation.getStreamUrl());
        mEditor.putString("LastRadioLogoBackgroundColor", mRadioStation.getLogoBackgroundColor());
        mEditor.putString("LastRadioBackdropBackgroundColor", mRadioStation.getBackdropBackgroundColor());
        mEditor.commit();
    }

    @Override
    public void buttonPlayPauseAction() {

        if (!Network.isConnected(mContext)) {

            mView.showSnakeBar("No Internet Connection");

        } else {

            if (sRadioStreamController.isPlayed()) {

                if (sRadioStreamController.getPlayStreamUrl().equals(mRadioStation.getStreamUrl())) {
                    sRadioStreamController.pause();
                    mView.showPlayIcon();
                } else {
                    sRadioStreamController.confirmSideExoPlayer();
                    sRadioStreamController.play();
                    saveAsLastPlayed();
                    mView.showPauseIcon();
                }

            } else {
                if (sRadioStreamController.getPlayStreamUrl() == null) {
                    sRadioStreamController.play();
                    saveAsLastPlayed();
                    mView.showPauseIcon();
                }
                if (sRadioStreamController.getPlayStreamUrl().equals(mRadioStation.getStreamUrl())) {
                    sRadioStreamController.play();
                    saveAsLastPlayed();
                    mView.showPauseIcon();
                } else {
                    sRadioStreamController.confirmSideExoPlayer();
                    sRadioStreamController.play();
                    saveAsLastPlayed();
                    mView.showPauseIcon();
                }
            }
        }


    }

    public void updatePlayPauseIcon() {
        if (sRadioStreamController.isPlayed()) {
            if (sRadioStreamController.getPlayStreamUrl().equals(mRadioStation.getStreamUrl())) {
                mView.showPauseIcon();
            } else {
                mView.showPlayIcon();
            }
        } else {
            mView.showPlayIcon();
        }
    }

    @Override
    public void volumeChanged() {
        if (!sRadioStreamController.isVolumeMute()) {
            int v = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            int max = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            mView.updateVolumeBar(v);
            mView.updateVolumeIcon(v, max);
        } else {
            mView.showVolumeMuteIcon();
        }
    }

    @Override
    public void changeVolume(int v) {
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, v, 0);
    }

    public void muteVolume() {
        sRadioStreamController.setVolumeMute(true);
        sRadioStreamController.setLastVolumeLevel(mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
        changeVolume(0);
    }

    public void unMuteVolume() {
        sRadioStreamController.setVolumeMute(false);
        changeVolume(sRadioStreamController.getLastVolumeLevel());
    }

    public void registerContentObserver() {
        mContext.getApplicationContext().getContentResolver().registerContentObserver(
                android.provider.Settings.System.CONTENT_URI, true,
                mSettingsContentObserver);
    }

    public void unregisterContentObserver() {
        mContext.getApplicationContext().getContentResolver().unregisterContentObserver(mSettingsContentObserver);
    }

    public void registerBroadcastReceiver() {
        IntentFilter intentFilter = new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION);
        mContext.getApplicationContext().registerReceiver(mNetworkStateChanged, intentFilter);
    }

    public void unregisterBroadcastReceiver() {
        mContext.getApplicationContext().unregisterReceiver(mNetworkStateChanged);
    }

    public class SettingsContentObserver extends ContentObserver {

        public SettingsContentObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            volumeChanged();
        }
    }

    public class NetworkStateChanged extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (Network.isConnected(context)) {

                if (mStartConnected){
                    if (!sRadioStreamController.isMainLoading()){
                        sRadioStreamController.rePrepareMainExoPlayer();
                        if(sRadioStreamController.isPlayed()&&mRadioStation.getStreamUrl().equals(sRadioStreamController.getPlayStreamUrl())){
                            mView.showPauseIcon();
                        }
                    }
                    if (!sRadioStreamController.isSideLoading()){
                        sRadioStreamController.rePrepareSideExoPlayer();
                    }
                }else {
                    prepareRadioStream();
                }


            } else if (intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, Boolean.FALSE)) {
                mView.showPlayIcon();
            }

        }

    }

}