package com.oze.emad.egyradio.MainAcivity.RadioStationsFragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oze.emad.egyradio.MainAcivity.BottomRadioPlayerListener;
import com.oze.emad.egyradio.R;
import com.oze.emad.egyradio.RadioActivity.RadioActivity;
import com.oze.emad.egyradio.Utilies.RadioStation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by 3mad on 21-Aug-17.
 */

public class RadioStationsFragment extends Fragment implements RadioStationsRecyclerViewAdapter.ItemListener {

    private FragmentActivity mContext;
    private View mRootView;
    private ArrayList<RadioStation> mRadioStations;
    private BottomRadioPlayerListener mBottomRadioPlayerListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = (FragmentActivity) context;
        mBottomRadioPlayerListener = (BottomRadioPlayerListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_radio_stations, container, false);

        mRadioStations = getRadioStations();
        initializeRecyclerView(mRadioStations);

        return mRootView;
    }

    public void initializeRecyclerView(ArrayList<RadioStation> radioStations) {
        RecyclerView mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.radio_stations_fragment_recyclerView);

        mRecyclerView.setHasFixedSize(true);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 2);
        mRecyclerView.setLayoutManager(gridLayoutManager);

        RadioStationsRecyclerViewAdapter adapter = new RadioStationsRecyclerViewAdapter(this, radioStations, mContext);
        mRecyclerView.setAdapter(adapter);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int maxBottomRadioPlayer = mBottomRadioPlayerListener.getBottomRadioPlayerHeight();
                if (dy > 0) {
                    //scroll up
                    mBottomRadioPlayerListener.setBottomRadioPlayerTranslationY(Math.min(maxBottomRadioPlayer, mBottomRadioPlayerListener.getBottomRadioPlayerTranslationY() + dy / 2));
                } else {
                    //scroll down
                    mBottomRadioPlayerListener.setBottomRadioPlayerTranslationY(Math.max(0, mBottomRadioPlayerListener.getBottomRadioPlayerTranslationY() + dy / 2));
                }
            }
        });
    }

    @Override
    public void OnClickListener(int position) {
        Intent intent = new Intent(mContext, RadioActivity.class);
        intent.putExtra("RadioStations", Parcels.wrap(mRadioStations.get(position)));
        startActivity(intent);
    }

    public String readJSONFromAsset() {
        String json = null;
        try {
            InputStream is = mContext.getAssets().open("EgyptRadioStations.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public ArrayList<RadioStation> getRadioStations() {

        ArrayList<RadioStation> radioStations = new ArrayList<>();

        try {

            JSONObject object = new JSONObject(readJSONFromAsset());
            JSONArray jsonArray = object.getJSONArray("Stations");

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject stationObject = jsonArray.getJSONObject(i);

                RadioStation radioStation = new RadioStation();

                radioStation.setName(stationObject.getString("Name"));
                radioStation.setStreamUrl(stationObject.getString("StreamUrl"));
                radioStation.setLogoUrl(stationObject.getString("LogoUrl"));
                radioStation.setLogoBackgroundColor(stationObject.getString("LogoBackgroundColor"));
                radioStation.setBackdropURL(stationObject.getString("BackdropURL"));
                radioStation.setBackdropBackgroundColor(stationObject.getString("BackdropBackgroundColor"));
                radioStation.setWebSiteUrl(stationObject.getString("websiteUrl"));
                radioStation.setDescription(stationObject.getString("Description"));

                radioStations.add(radioStation);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return radioStations;
    }

}
