package com.oze.emad.egyradio.MainAcivity.FavouriteFragment;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.github.florent37.glidepalette.BitmapPalette;
import com.github.florent37.glidepalette.GlidePalette;
import com.oze.emad.egyradio.R;
import com.oze.emad.egyradio.Utilies.RadioStation;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by 3mad on 22-Aug-17.
 */

public class FavouriteRecyclerViewAdapter extends RecyclerView.Adapter<FavouriteRecyclerViewAdapter.ViewHolder> {

    private ItemListener mItemListener;
    private ArrayList<RadioStation> mRadioStations;
    private Context mContext;

    public FavouriteRecyclerViewAdapter(ItemListener itemListener, ArrayList<RadioStation> radioStations, Context context) {
        this.mItemListener = itemListener;
        mRadioStations = radioStations;
        mContext = context;
    }

    interface ItemListener {
        void OnClickListener(int position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.radio_item,parent,false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

//        Glide.with(mContext).load(mRadioStations.get(position).getLogoUrl())
//                .listener(GlidePalette.with(mRadioStations.get(position).getLogoUrl())
//                        .use(GlidePalette.Profile.VIBRANT)
//                        .intoBackground(holder.mImageView, BitmapPalette.Swatch.RGB)
//                )
//                .into(holder.mImageView);
        holder.mImageView.setBackgroundColor(Color.parseColor(mRadioStations.get(position).getLogoBackgroundColor()));
        Picasso.with(mContext).load(mRadioStations.get(position).getLogoUrl())
                .placeholder(R.drawable.logo1)
                .into(holder.mImageView);

    }

    @Override
    public int getItemCount() {
        return mRadioStations.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView mImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.radio_item_imageView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mItemListener.OnClickListener(getLayoutPosition());
                }
            });
        }
    }
}
