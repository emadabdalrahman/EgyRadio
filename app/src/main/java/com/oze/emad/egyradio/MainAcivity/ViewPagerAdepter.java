package com.oze.emad.egyradio.MainAcivity;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.oze.emad.egyradio.MainAcivity.FavouriteFragment.FavouriteFragment;
import com.oze.emad.egyradio.MainAcivity.RadioStationsFragment.RadioStationsFragment;

/**
 * Created by 3mad on 21-Aug-17.
 */

public class ViewPagerAdepter extends FragmentStatePagerAdapter {

    Fragment[] fragments = {
            new RadioStationsFragment(),
            new FavouriteFragment()
    };

    String[] Titles = {
            "RadioStations",
            "Favourite"
    };

    public ViewPagerAdepter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }
}
