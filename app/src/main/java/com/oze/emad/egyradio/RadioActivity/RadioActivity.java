package com.oze.emad.egyradio.RadioActivity;

import android.content.ContentValues;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SlidingPaneLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.oze.emad.egyradio.Model.Local.DbContract;
import com.oze.emad.egyradio.R;
import com.oze.emad.egyradio.RadioActivity.SpeedList.SpeedListFragment;
import com.oze.emad.egyradio.Utilies.RadioStation;

import org.parceler.Parcels;

public class RadioActivity extends AppCompatActivity implements SpeedListFragment.SpeedListListener {

    private RadioStation mRadioStation;
    private Menu mMenu;
    private BottomSheetBehavior mBottomSheetBehavior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRadioStation = Parcels.unwrap(getIntent().getParcelableExtra("RadioStations"));

        setContentView(R.layout.activity_radio);

        initializeToolbar();
        initializeBottomSheet();

       if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP ) {
            FrameLayout frameLayout = (FrameLayout) findViewById(R.id.radio_activity_speed_list_frame);
            frameLayout.setPadding(0, getStatusBarHeight(), 0, 0);
        }

        startRadioFragment(mRadioStation);
        startSpeedListRadioFragment();

    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public void initializeToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.radio_activity_toolbar);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP ){
            AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.radio_activity_appBar);
            appBarLayout.setPadding(0,getStatusBarHeight(),0,0);
        }

        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(mRadioStation.getName());

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void startRadioFragment(RadioStation radioStation) {

        RadioFragment radioFragment = new RadioFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelable("RadioStation", Parcels.wrap(radioStation));
        radioFragment.setArguments(bundle);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.radio_activity_radio_controller_frame, radioFragment).commit();

    }

    public void startSpeedListRadioFragment() {

        SpeedListFragment speedListFragment = new SpeedListFragment();

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.radio_activity_speed_list_frame, speedListFragment).commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.radio_station_menu, menu);
        mMenu = menu;
        if (isSpeedList()) {
            showSpeedListIcon();
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.radio_details:

                if (isBottomSheetOpen()) {
                    closeBottomSheet();
                }
                if (isBottomSheetClose()) {
                    openBottomSheet();
                }

                return true;

            case R.id.radio_speed_list:

                if (!isSpeedList()) {
                    addToSpeedList();
                    showSpeedListIcon();
                    startSpeedListRadioFragment();
                } else {
                    removeFromSpeedList();
                    showUnSpeedListIcon();
                    startSpeedListRadioFragment();
                }

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void updateToolbar(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void updateRadioStation(RadioStation radioStation) {
        mRadioStation = radioStation;
    }

    @Override
    public boolean isSpeedListOpened() {
        SlidingPaneLayout slidingPaneLayout = (SlidingPaneLayout) findViewById(R.id.activity_radio_sliding_panel);
        return slidingPaneLayout.isOpen();
    }

    @Override
    public void updateMenuIcon() {
        showSpeedListIcon();
    }

    public void addToSpeedList() {

        ContentValues values = new ContentValues();

        values.put(DbContract.RadioStation.COLUMN_NAME, mRadioStation.getName());
        values.put(DbContract.RadioStation.COLUMN_STREAM_URL, mRadioStation.getStreamUrl());
        values.put(DbContract.RadioStation.COLUMN_WEB_SITE_URL, mRadioStation.getWebSiteUrl());
        values.put(DbContract.RadioStation.COLUMN_LOGO_URL, mRadioStation.getLogoUrl());
        values.put(DbContract.RadioStation.COLUMN_BACKDROP_URL, mRadioStation.getBackdropURL());
        values.put(DbContract.RadioStation.COLUMN_DESCRIPTION, mRadioStation.getDescription());
        values.put(DbContract.RadioStation.COLUMN_BACKDROP_BACKGROUND_COLOR, mRadioStation.getBackdropBackgroundColor());
        values.put(DbContract.RadioStation.COLUMN_LOGO_BACKGROUND_COLOR, mRadioStation.getLogoBackgroundColor());

        getContentResolver().insert(DbContract.RadioStation.CONTENT_SPEED_LIST_URI, values);
    }

    public boolean isSpeedList() {
        Cursor cursor = getContentResolver().query(DbContract.RadioStation.CONTENT_SPEED_LIST_URI, null, null, null, null);
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                if (cursor.getString(cursor.getColumnIndex(DbContract.RadioStation.COLUMN_NAME)).equals(mRadioStation.getName())) {
                    return true;
                }
            }
        }
        return false;
    }

    public void removeFromSpeedList() {
        Uri uri = DbContract.RadioStation.CONTENT_SPEED_LIST_URI.buildUpon().appendPath(mRadioStation.getName()).build();
        getContentResolver().delete(uri, null, null);
    }

    public void showSpeedListIcon() {
        mMenu.getItem(1).setIcon(R.drawable.ic_queue_music_red_48dp);
    }

    public void showUnSpeedListIcon() {
        mMenu.getItem(1).setIcon(R.drawable.ic_queue_music_white_48dp);
    }

    public void initializeBottomSheet() {
        View view = findViewById(R.id.radio_activity_bottom_sheet_view);
        mBottomSheetBehavior = BottomSheetBehavior.from(view);
        mBottomSheetBehavior.setPeekHeight(0);
        TextView radioStationDescription = (TextView) findViewById(R.id.radio_activity_description);
        radioStationDescription.setText(mRadioStation.getDescription());
    }

    public void openBottomSheet() {
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    public void closeBottomSheet() {
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    public boolean isBottomSheetOpen() {
        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            return true;
        }
        return false;
    }

    public boolean isBottomSheetClose() {
        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
            return true;
        }
        return false;
    }

}
