package com.oze.emad.egyradio.Model.Local;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by 3mad on 25-Aug-17.
 */

public class DbContentProvider extends ContentProvider {


    public static final int FAVORITE_RADIO_STATION = 100;
    public static final int FAVORITE_RADIO_STATION_WITH_NAME = 101;
    public static final int SPEED_LIST_RADIO_STATION = 200;
    public static final int SPEED_LIST_RADIO_STATION_WITH_NAME= 201;
    private static DbHelper sDbHelper = null;
    private static final UriMatcher sURI_MATCHER = buildUriMatcher();

    public static UriMatcher buildUriMatcher() {
        UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(DbContract.AUTHORITY, DbContract.PATH_FAVORITE_RADIO_STATIONS, FAVORITE_RADIO_STATION);
        uriMatcher.addURI(DbContract.AUTHORITY, DbContract.PATH_FAVORITE_RADIO_STATIONS + "/*", FAVORITE_RADIO_STATION_WITH_NAME);
        uriMatcher.addURI(DbContract.AUTHORITY, DbContract.PATH_SPEED_LIST_RADIO_STATIONS, SPEED_LIST_RADIO_STATION);
        uriMatcher.addURI(DbContract.AUTHORITY, DbContract.PATH_SPEED_LIST_RADIO_STATIONS+ "/*", SPEED_LIST_RADIO_STATION_WITH_NAME);
        return uriMatcher;
    }

    @Override
    public boolean onCreate() {
        if (sDbHelper == null) {
            sDbHelper = new DbHelper(getContext());
        }
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {

        SQLiteDatabase sqLiteDatabase = sDbHelper.getReadableDatabase();

        Cursor returnCursor = null;

        switch (sURI_MATCHER.match(uri)) {
            case FAVORITE_RADIO_STATION:
                returnCursor = sqLiteDatabase.query(DbContract.RadioStation.FAVOURITE_TABLE_NAME, DbContract.RadioStation.ALL_COLUMNS, null, null, null, null, null);
                break;
            case SPEED_LIST_RADIO_STATION:
                returnCursor = sqLiteDatabase.query(DbContract.RadioStation.SPEED_LIST_TABLE_NAME, DbContract.RadioStation.ALL_COLUMNS, null, null, null, null, null);
                break;
            default:
        }

        return returnCursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {

        SQLiteDatabase sqLiteDatabase = sDbHelper.getWritableDatabase();

        long id;
        Uri returnUri = null;

        switch (sURI_MATCHER.match(uri)) {
            case FAVORITE_RADIO_STATION:

                id = sqLiteDatabase.insert(DbContract.RadioStation.FAVOURITE_TABLE_NAME, null, values);
                returnUri = ContentUris.withAppendedId(uri, id);
                break;
            case SPEED_LIST_RADIO_STATION:

                id = sqLiteDatabase.insert(DbContract.RadioStation.SPEED_LIST_TABLE_NAME, null, values);
                returnUri = ContentUris.withAppendedId(uri, id);
                break;
            default:

        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {

        SQLiteDatabase sqLiteDatabase = sDbHelper.getWritableDatabase();
        int numOfDeletedRows = 0;
        String radioName;
        String mSelection;

        switch (sURI_MATCHER.match(uri)) {
            case FAVORITE_RADIO_STATION_WITH_NAME:
                radioName = uri.getPathSegments().get(1);

                mSelection = DbContract.RadioStation.COLUMN_NAME + "='" + radioName +"'";
                numOfDeletedRows = sqLiteDatabase.delete(DbContract.RadioStation.FAVOURITE_TABLE_NAME, mSelection , null);

                break;
            case SPEED_LIST_RADIO_STATION_WITH_NAME:
                radioName = uri.getPathSegments().get(1);

                mSelection = DbContract.RadioStation.COLUMN_NAME + "='" + radioName +"'";
                numOfDeletedRows = sqLiteDatabase.delete(DbContract.RadioStation.SPEED_LIST_TABLE_NAME, mSelection , null);

                break;
            default:

        }
        return numOfDeletedRows;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
