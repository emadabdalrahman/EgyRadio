package com.oze.emad.egyradio.RadioActivity.SpeedList;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oze.emad.egyradio.Model.Local.DbContract;
import com.oze.emad.egyradio.R;
import com.oze.emad.egyradio.RadioActivity.RadioFragment;
import com.oze.emad.egyradio.Utilies.RadioStation;

import org.parceler.Parcels;

import java.util.ArrayList;

/**
 * Created by 3mad on 23-Aug-17.
 */

public class SpeedListFragment extends Fragment implements SpeedListRecyclerViewAdepter.ItemListener{

    private View mRootView;
    private FragmentActivity mContext;
    private ArrayList<RadioStation> mSpeedList;
    private SpeedListListener mSpeedListListener;
    private  RecyclerView mRecyclerView;


    public interface SpeedListListener{
        void updateToolbar(String title);
        void updateRadioStation(RadioStation radioStation);
        boolean isSpeedListOpened();
        void updateMenuIcon();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = (FragmentActivity) context;
        mSpeedListListener = (SpeedListListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_speed_list, container, false);

        mSpeedList = getSpeedListRadioStations();

       initializeRecyclerView();

        return mRootView;
    }

    public void initializeRecyclerView(){

        mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.speed_list_fragment_recyclerView);
        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        SpeedListRecyclerViewAdepter adepter = new SpeedListRecyclerViewAdepter(this, mSpeedList , mContext);
        mRecyclerView.setAdapter(adepter);

    }

    public ArrayList<RadioStation> getSpeedListRadioStations() {

        Cursor cursor = mContext.getContentResolver().query(DbContract.RadioStation.CONTENT_SPEED_LIST_URI,null,null,null,null);

        ArrayList<RadioStation> radioStations = new ArrayList<>();

        if (cursor.getCount()>0){
            while (cursor.moveToNext()){
                RadioStation radioStation = new RadioStation();
                radioStation.setName(cursor.getString(cursor.getColumnIndex(DbContract.RadioStation.COLUMN_NAME)));
                radioStation.setStreamUrl(cursor.getString(cursor.getColumnIndex(DbContract.RadioStation.COLUMN_STREAM_URL)));
                radioStation.setWebSiteUrl(cursor.getString(cursor.getColumnIndex(DbContract.RadioStation.COLUMN_WEB_SITE_URL)));
                radioStation.setLogoUrl(cursor.getString(cursor.getColumnIndex(DbContract.RadioStation.COLUMN_LOGO_URL)));
                radioStation.setLogoBackgroundColor(cursor.getString(cursor.getColumnIndex(DbContract.RadioStation.COLUMN_LOGO_BACKGROUND_COLOR)));
                radioStation.setBackdropURL(cursor.getString(cursor.getColumnIndex(DbContract.RadioStation.COLUMN_BACKDROP_URL)));
                radioStation.setBackdropBackgroundColor(cursor.getString(cursor.getColumnIndex(DbContract.RadioStation.COLUMN_BACKDROP_BACKGROUND_COLOR)));
                radioStation.setDescription(cursor.getString(cursor.getColumnIndex(DbContract.RadioStation.COLUMN_DESCRIPTION)));

                radioStations.add(radioStation);
            }
        }

        return radioStations;
    }

    public void startRadioFragment(RadioStation radioStation) {

        RadioFragment radioFragment = new RadioFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelable("RadioStation", Parcels.wrap(radioStation));
        radioFragment.setArguments(bundle);

        FragmentManager fragmentManager = mContext.getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.radio_activity_radio_controller_frame, radioFragment).commit();

    }

    @Override
    public void OnClickListener(int position) {
        if (mSpeedListListener.isSpeedListOpened()){
            startRadioFragment(mSpeedList.get(position));
            mSpeedListListener.updateToolbar(mSpeedList.get(position).getName());
            mSpeedListListener.updateRadioStation(mSpeedList.get(position));
            mSpeedListListener.updateMenuIcon();
        }
    }

}
