package com.oze.emad.egyradio.MainAcivity.FavouriteFragment;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oze.emad.egyradio.MainAcivity.BottomRadioPlayerListener;
import com.oze.emad.egyradio.Model.Local.DbContract;
import com.oze.emad.egyradio.R;
import com.oze.emad.egyradio.RadioActivity.RadioActivity;
import com.oze.emad.egyradio.Utilies.RadioStation;

import org.parceler.Parcels;

import java.util.ArrayList;

/**
 * Created by 3mad on 21-Aug-17.
 */

public class FavouriteFragment extends Fragment implements FavouriteRecyclerViewAdapter.ItemListener {
    private FragmentActivity mContext;
    private View mRootView;
    private ArrayList<RadioStation> mRadioStations;
    private NestedScrollView mEmptyLayout;
    private BottomRadioPlayerListener mBottomRadioPlayerListener;
    public static boolean sNotifyDbChanged;
    public RecyclerView mRecyclerView;
    public FavouriteRecyclerViewAdapter mFavouriteRecyclerViewAdapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = (FragmentActivity) context;
        mBottomRadioPlayerListener = (BottomRadioPlayerListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_favourite, container, false);
        mEmptyLayout = (NestedScrollView) mRootView.findViewById(R.id.favourite_fragment_empty_layout);
        initializeEmptyLayout();
        if (isEmpty()) {
            showEmptyLayout();
        } else {
            hiddenEmptyLayout();
            mRadioStations = getFavoriteRadioStations();
            initializeRecyclerView(mRadioStations);
        }

        return mRootView;
    }

    public void initializeRecyclerView(ArrayList<RadioStation> radioStations) {
        mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.favourite_fragment_recyclerView);

        mRecyclerView.setHasFixedSize(true);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 2);
        mRecyclerView.setLayoutManager(gridLayoutManager);

        mFavouriteRecyclerViewAdapter = new FavouriteRecyclerViewAdapter(this, radioStations, mContext);
        mRecyclerView.setAdapter(mFavouriteRecyclerViewAdapter);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int maxBottomRadioPlayer = mBottomRadioPlayerListener.getBottomRadioPlayerHeight();
                if (dy > 0) {
                    //scroll up
                    mBottomRadioPlayerListener.setBottomRadioPlayerTranslationY(Math.min(maxBottomRadioPlayer, mBottomRadioPlayerListener.getBottomRadioPlayerTranslationY() + dy / 2));
                } else {
                    //scroll down
                    mBottomRadioPlayerListener.setBottomRadioPlayerTranslationY(Math.max(0, mBottomRadioPlayerListener.getBottomRadioPlayerTranslationY() + dy / 2));
                }
            }
        });
    }

    public ArrayList<RadioStation> getFavoriteRadioStations() {
        Cursor cursor = mContext.getContentResolver().query(DbContract.RadioStation.CONTENT_FAVORITE_URI, null, null, null, null);
        ArrayList<RadioStation> radioStations = new ArrayList<>();

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                RadioStation radioStation = new RadioStation();
                radioStation.setName(cursor.getString(cursor.getColumnIndex(DbContract.RadioStation.COLUMN_NAME)));
                radioStation.setStreamUrl(cursor.getString(cursor.getColumnIndex(DbContract.RadioStation.COLUMN_STREAM_URL)));
                radioStation.setWebSiteUrl(cursor.getString(cursor.getColumnIndex(DbContract.RadioStation.COLUMN_WEB_SITE_URL)));
                radioStation.setLogoUrl(cursor.getString(cursor.getColumnIndex(DbContract.RadioStation.COLUMN_LOGO_URL)));
                radioStation.setLogoBackgroundColor(cursor.getString(cursor.getColumnIndex(DbContract.RadioStation.COLUMN_LOGO_BACKGROUND_COLOR)));
                radioStation.setBackdropURL(cursor.getString(cursor.getColumnIndex(DbContract.RadioStation.COLUMN_BACKDROP_URL)));
                radioStation.setBackdropBackgroundColor(cursor.getString(cursor.getColumnIndex(DbContract.RadioStation.COLUMN_BACKDROP_BACKGROUND_COLOR)));
                radioStation.setDescription(cursor.getString(cursor.getColumnIndex(DbContract.RadioStation.COLUMN_DESCRIPTION)));
                radioStations.add(radioStation);
            }
        }

        return radioStations;
    }

    @Override
    public void onResume() {
        super.onResume();
        notifyDbChanged();
    }

    public void notifyDbChanged() {
        if (sNotifyDbChanged) {
            if (isEmpty()) {
                showEmptyLayout();
            }else{
                if (mEmptyLayout.getVisibility() == View.VISIBLE) {
                    hiddenEmptyLayout();
                    mRadioStations = getFavoriteRadioStations();
                    initializeRecyclerView(mRadioStations);
                } else {
                    mRadioStations = getFavoriteRadioStations();
                    mFavouriteRecyclerViewAdapter = new FavouriteRecyclerViewAdapter(this, mRadioStations, mContext);
                    mRecyclerView.setAdapter(mFavouriteRecyclerViewAdapter);
                }
            }
            sNotifyDbChanged = false;
        }
    }

    public boolean isEmpty() {
        Cursor cursor = mContext.getContentResolver().query(DbContract.RadioStation.CONTENT_FAVORITE_URI, null, null, null, null);
        if (cursor.getCount() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public void initializeEmptyLayout() {
        mEmptyLayout.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                int maxBottomRadioPlayer = mBottomRadioPlayerListener.getBottomRadioPlayerHeight();
                float x = (int) mBottomRadioPlayerListener.getBottomRadioPlayerTranslationY();
                int y = scrollY - oldScrollY;
                if (y > 0) {
                    //scroll up
                    mBottomRadioPlayerListener.setBottomRadioPlayerTranslationY(Math.min(maxBottomRadioPlayer, mBottomRadioPlayerListener.getBottomRadioPlayerTranslationY() + y / 2));
                } else {
                    //scroll down
                    mBottomRadioPlayerListener.setBottomRadioPlayerTranslationY(Math.max(0, mBottomRadioPlayerListener.getBottomRadioPlayerTranslationY() + y / 2));
                }
            }
        });
    }

    public void showEmptyLayout() {
        if (mEmptyLayout.getVisibility() == View.INVISIBLE) {
            mEmptyLayout.setVisibility(View.VISIBLE);
        }
    }

    public void hiddenEmptyLayout() {
        if (mEmptyLayout.getVisibility() == View.VISIBLE) {
            mEmptyLayout.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void OnClickListener(int position) {
        Intent intent = new Intent(mContext, RadioActivity.class);
        intent.putExtra("RadioStations", Parcels.wrap(mRadioStations.get(position)));
        startActivity(intent);
    }
}
