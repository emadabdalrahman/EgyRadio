package com.oze.emad.egyradio.Utilies;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.Display;

/**
 * Created by 3mad on 02-Oct-17.
 */

public class Screen {
    private double width_dp;
    private double height_dp;

    public void ScreenSize(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();

        DisplayMetrics displayMetrics =new DisplayMetrics();
        display.getMetrics(displayMetrics);

        Float density = activity.getResources().getDisplayMetrics().density;
        height_dp = displayMetrics.heightPixels/density;
        width_dp = displayMetrics.widthPixels/density;
    }
    public double getWidth_dp(){return width_dp;}
    public double getHeight_dp(){return height_dp;}
}
