package com.oze.emad.egyradio.MainAcivity;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.florent37.glidepalette.BitmapPalette;
import com.github.florent37.glidepalette.GlidePalette;
import com.mxn.soul.flowingdrawer_core.ElasticDrawer;
import com.mxn.soul.flowingdrawer_core.FlowingDrawer;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.oze.emad.egyradio.R;
import com.oze.emad.egyradio.Utilies.RadioStation;
import com.oze.emad.egyradio.Utilies.RadioStreamController;
import com.readystatesoftware.systembartint.SystemBarTintManager;
import com.squareup.picasso.Picasso;

import net.grobas.view.MovingImageView;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, BottomRadioPlayerListener {

    private ViewPager mViewPager;
    private Toolbar mToolbar;
    private int minViewPagerHeight;
    public static RadioStreamController sRadioStreamController = null;
    public static SharedPreferences sSharedPreferences = null;
    private AppBarLayout mAppBarLayout;
    private DrawerLayout drawerLayout;
    private RelativeLayout mMainBottomRadioFrame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_v2);

        initializeRadioStreamController();
        initializeToolbar();
        initializeViewPager();
        initializeTabs();
        initializeNavigationView();
        initializeSharedPreferences();
       // initializeAppBarBackground();
        //     int x = getStatusBarHeight();

//        // create our manager instance after the content view is set
//        SystemBarTintManager tintManager = new SystemBarTintManager(this);
//        // enable status bar tint
//        tintManager.setStatusBarTintEnabled(true);
//        // enable navigation bar tint
//        tintManager.setNavigationBarTintEnabled(true);
//        // set a custom tint color for all system bars
//        tintManager.setTintColor(Color.parseColor("#20000000"));
        // set a custom navigation bar resource
        //   tintManager.setNavigationBarTintResource(R.drawable.my_tint);
        // set a custom status bar drawable
        // tintManager.setStatusBarTintDrawable(drawerLayout);

        mMainBottomRadioFrame = (RelativeLayout) findViewById(R.id.main_bottom_radio_player_frame);
        mAppBarLayout = (AppBarLayout) findViewById(R.id.main_app_bar_layout);

        minViewPagerHeight = mViewPager.getHeight();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("main", "pause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("main", "stop");
    }

    @Override
    protected void onResume() {
        super.onResume();
        RadioStation radioStation = getLastRadioPlayed();
        //    if (isLastRadioPlayedEmpty(radioStation)){

        //    }else {
        showBottomRadioPlayerControllerData(radioStation);
        //    }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public void initializeAppBarBackground() {
//        MovingImageView movingImageView = (MovingImageView) findViewById(R.id.main_app_bar_moving_background);
//        movingImageView.getMovingAnimator().setInterpolator(new DecelerateInterpolator());
//        movingImageView.getMovingAnimator().setSpeed(1);
//        movingImageView.setMaxRelativeSize(50);
//        movingImageView.getMovingAnimator().addCustomMovement().
//                addHorizontalMoveToLeft().
//                addHorizontalMoveToRight().
//                start();
//
//        movingImageView.setBackgroundColor(Color.parseColor(backgroundColor));

       // AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.main_app_bar_layout);
       // appBarLayout.setPadding(0, getStatusBarHeight(),0, 0);

        ImageView movingImageView = (ImageView) findViewById(R.id.main_app_bar_moving_background);
        // movingImageView.setBackgroundColor(Color.parseColor(backgroundColor));

        Picasso.with(this)
                .load(R.drawable.back)
                .into(movingImageView);
    }

    public void initializeToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        //   mToolbar.setPadding(0, getStatusBarHeight(), 0, 0);

        //  mToolbar.setLayoutParams();
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(true);
    }

    public void initializeViewPager() {
        mViewPager = (ViewPager) findViewById(R.id.main_viewPager);
        ViewPagerAdepter adepter = new ViewPagerAdepter(getSupportFragmentManager());

        mViewPager.setAdapter(adepter);
    }

    public void initializeTabs() {
        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(mViewPager);
    }

    public void initializeFlowingDrawer() {
        FlowingDrawer mDrawer = (FlowingDrawer) findViewById(R.id.drawerlayout);
        mDrawer.setTouchMode(ElasticDrawer.TOUCH_MODE_BEZEL);
        mDrawer.setOnDrawerStateChangeListener(new ElasticDrawer.OnDrawerStateChangeListener() {
            @Override
            public void onDrawerStateChange(int oldState, int newState) {
                if (newState == ElasticDrawer.STATE_CLOSED) {
                    Log.i("MainActivity", "Drawer STATE_CLOSED");
                }
            }

            @Override
            public void onDrawerSlide(float openRatio, int offsetPixels) {
                Log.i("MainActivity", "openRatio=" + openRatio + " ,offsetPixels=" + offsetPixels);
            }
        });
    }

    public void initializeNavigationView() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void initializeRadioStreamController() {
        if (sRadioStreamController == null) {
            sRadioStreamController = new RadioStreamController(this);
        }
    }

    public void initializeSharedPreferences() {

        if (sSharedPreferences == null) {
            sSharedPreferences = getSharedPreferences("EgyRadioSetting", MODE_PRIVATE);
        }

    }

    public RadioStation getLastRadioPlayed() {
        RadioStation radioStation = new RadioStation();
        radioStation.setName(sSharedPreferences.getString("LastRadioName", null));
        radioStation.setLogoUrl(sSharedPreferences.getString("LastRadioLogoUrl", null));
        radioStation.setBackdropURL(sSharedPreferences.getString("LastRadioBackdropURL", null));
        radioStation.setStreamUrl(sSharedPreferences.getString("LastRadioStreamUrl", null));
        radioStation.setLogoBackgroundColor(sSharedPreferences.getString("LastRadioLogoBackgroundColor", null));
        radioStation.setBackdropBackgroundColor(sSharedPreferences.getString("LastRadioBackdropBackgroundColor", null));
        return radioStation;
    }

    public boolean isLastRadioPlayedEmpty(RadioStation radioStation) {
        if (radioStation.getStreamUrl() == null &&
                radioStation.getLogoUrl() == null &&
                radioStation.getLogoBackgroundColor() == null &&
                radioStation.getName() == null &&
                radioStation.getBackdropBackgroundColor() == null &&
                radioStation.getBackdropURL() == null) {
            return true;
        } else {
            return false;
        }
    }

    public void showBottomRadioPlayerName(String name) {
        TextView radioName = (TextView) findViewById(R.id.main_bottom_radio_player_name);
        if (name != null) {
            radioName.setText(name);
        }
    }

    public void showBottomRadioPlayerLogo(String LogoUrl, String LogoBackgroundColor) {
        ImageView radioImage = (ImageView) findViewById(R.id.main_bottom_radio_player_image);
        radioImage.setBackgroundColor(Color.parseColor(LogoBackgroundColor));
        if (LogoUrl != null) {

            Picasso.with(this)
                    .load(LogoUrl)
                    .placeholder(R.drawable.logo1)
                    .into(radioImage);
        }
    }

    public void showBottomRadioPlayerControllerData(RadioStation radioStation) {

        // initializeAppBarBackground(radioStation.getBackdropURL(), radioStation.getBackdropBackgroundColor());

        if (radioStation.getName() == null && radioStation.getLogoUrl() == null &&
                radioStation.getBackdropURL() == null && radioStation.getStreamUrl() == null) {
            View view = (View) findViewById(R.id.main_bottom_radio_player_frame);
            view.setVisibility(View.INVISIBLE);
        } else {

            showBottomRadioPlayerLogo(radioStation.getLogoUrl(), radioStation.getLogoBackgroundColor());

            showBottomRadioPlayerName(radioStation.getName());

            final ImageButton radioController = (ImageButton) findViewById(R.id.main_bottom_radio_player_play_pause_button);
            if (sRadioStreamController.getPlayStreamUrl() == null) {
                radioController.setImageResource(R.drawable.ic_play_arrow_black_48dp);
            } else if (radioStation.getStreamUrl().equals(sRadioStreamController.getPlayStreamUrl())) {
                if (sRadioStreamController.isPlayed()) {
                    radioController.setImageResource(R.drawable.ic_pause_black_48dp);
                } else {
                    radioController.setImageResource(R.drawable.ic_play_arrow_black_48dp);
                }
            }

            if (!sRadioStreamController.isPlayed()) {
                sRadioStreamController.prepareMainExoPlayer(radioStation.getStreamUrl());
            }

            radioController.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (sRadioStreamController.isPlayed()) {
                        sRadioStreamController.pause();
                        radioController.setImageResource(R.drawable.ic_play_arrow_black_48dp);
                    } else {
                        sRadioStreamController.play();
                        radioController.setImageResource(R.drawable.ic_pause_black_48dp);
                    }
                }
            });
        }

    }

    @Override
    public int getBottomRadioPlayerHeight() {
        return mMainBottomRadioFrame.getHeight();
    }

    @Override
    public void setBottomRadioPlayerTranslationY(float translationY) {
        mMainBottomRadioFrame.setTranslationY(translationY);
    }

    @Override
    public float getBottomRadioPlayerTranslationY() {
        return mMainBottomRadioFrame.getTranslationY();
    }

}
