package com.oze.emad.egyradio.Utilies;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.net.Uri;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ext.okhttp.OkHttpDataSourceFactory;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.util.Util;

import okhttp3.OkHttpClient;

/**
 * Created by 3mad on 16-Sep-17.
 */

public class RadioStreamController implements OnAudioFocusChangeListener {

    private final int BUFFER_SEGMENT_SIZE = 64 * 1024;
    private final int BUFFER_SEGMENT_COUNT = 256;
    private String mPlayStreamUrl = null;
    private SimpleExoPlayer mMainExoPlayer = null;
    private SimpleExoPlayer mSideExoPlayer = null;
    private int mAudioFocusState = -1;
    private String mSideStreamUrl = null;
    private Context mContext;
    private AudioManager mAudioManager = null;
    private Boolean streamPlayed = false;
    private boolean volumeMute;
    private int lastVolumeLevel;
    private boolean mMainPrepared = false;
    private boolean mSidePrepared = false;


    public RadioStreamController(Context mContext) {
        this.mContext = mContext;
        initializeAudioManager();
        initializeMainExoPlayer();
        initializeSideExoPlayer();
    }

    public void initializeAudioManager() {

        if (mAudioManager == null) {
            mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        }

        if (mAudioFocusState == -1) {

            mAudioFocusState = mAudioManager.requestAudioFocus(
                    this,
                    AudioManager.STREAM_MUSIC,
                    AudioManager.AUDIOFOCUS_GAIN);

        }

    }

    public SimpleExoPlayer initializeExoPlayer(){
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory radioTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(radioTrackSelectionFactory);
        return ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(mContext), trackSelector,new DefaultLoadControl());
    }

    public void initializeSideExoPlayer() {

        if (mSideExoPlayer == null) {
            mSideExoPlayer = initializeExoPlayer();
        }

    }

    public void initializeMainExoPlayer() {

        if (mMainExoPlayer == null) {
           mMainExoPlayer = initializeExoPlayer();
        }

    }

    public MediaSource settingExoPlayer(String streamUrl) {

        Uri radioUri = Uri.parse(streamUrl);

        // Settings for exoPlayer r1
//        Allocator allocator = new DefaultAllocator(BUFFER_SEGMENT_SIZE);
//        String userAgent = Util.getUserAgent(mContext, "EgyRadio");
//        DataSource dataSource = new DefaultUriDataSource(mContext, null, userAgent);
//        ExtractorSampleSource sampleSource = new ExtractorSampleSource(
//                radioUri, dataSource, allocator, BUFFER_SEGMENT_SIZE * BUFFER_SEGMENT_COUNT);
//        TrackRenderer audioRenderer = new MediaCodecAudioTrackRenderer(sampleSource);

        MediaSource source = new ExtractorMediaSource(
                radioUri,
                new OkHttpDataSourceFactory(
                        new OkHttpClient(),
                        Util.getUserAgent(mContext, "Egy Radio"),
                        null
                ),
                new DefaultExtractorsFactory(),
                null,
                null
        );

        return source;
    }

    public void prepareMainExoPlayer(String streamURL) {
        // Prepare Main ExoPlayer
        mMainExoPlayer.prepare(settingExoPlayer(streamURL));
        mPlayStreamUrl = streamURL;
        mMainPrepared = true;
    }

    public void prepareSideExoPlayer(String streamURL) {
        // Prepare Side ExoPlayer
        mSideExoPlayer.prepare(settingExoPlayer(streamURL));
        mSideStreamUrl = streamURL;
        mSidePrepared = true;
    }

    public void rePrepareMainExoPlayer(){
        if (mPlayStreamUrl != null){
            mMainExoPlayer.prepare(settingExoPlayer(mPlayStreamUrl));
            mMainPrepared = true;
        }
    }

    public void rePrepareSideExoPlayer(){
        if (mSideStreamUrl != null){
            mSideExoPlayer.prepare(settingExoPlayer(mSideStreamUrl));
            mSidePrepared = true;
        }
    }

    public void confirmSideExoPlayer() {
        mMainExoPlayer.release();
        mMainExoPlayer = mSideExoPlayer;
        mSideExoPlayer = null;
        initializeSideExoPlayer();
        mPlayStreamUrl = mSideStreamUrl;
        mSidePrepared = false;
    }

    public boolean isMainPrepared(){
        return mMainPrepared;
    }

    public boolean isSidePrepared(){
        return mSidePrepared;
    }

    public boolean isSideLoading(){
        return mSideExoPlayer.isLoading();
    }

    public boolean isMainLoading(){
        return mMainExoPlayer.isLoading();
    }

    public void stopPrepareSideExoPlayer() {
        mSideExoPlayer.stop();
    }

    public void play() {
        if (mAudioFocusState == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            mMainExoPlayer.setPlayWhenReady(true);
            streamPlayed = true;
        }
    }

    public void pause() {
        if (mAudioFocusState == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            mMainExoPlayer.setPlayWhenReady(false);
        }
    }

    public boolean isPlayed() {
        return mMainExoPlayer.getPlayWhenReady();
    }

    public String getPlayStreamUrl() {
        return mPlayStreamUrl;
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        if (streamPlayed) {

            if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {
                mMainExoPlayer.setPlayWhenReady(false);
            }
            if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                mMainExoPlayer.setPlayWhenReady(true);
            }
            if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                mMainExoPlayer.setPlayWhenReady(false);
            }

        }
    }

    public boolean isVolumeMute() {
        return volumeMute;
    }

    public void setVolumeMute(boolean volumeMute) {
        this.volumeMute = volumeMute;
    }

    public int getLastVolumeLevel() {
        return lastVolumeLevel;
    }

    public void setLastVolumeLevel(int lastVolumeLevel) {
        this.lastVolumeLevel = lastVolumeLevel;
    }
}

